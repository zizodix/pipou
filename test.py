# Import non utilisé
import json

def calcul_inutile():
    a = 5
    b = 10
    # Opération inutile
    c = a + b
    print("Ceci est un calcul inutile.")

def demonstration_duplication():
    liste = [1, 2, 3, 4, 5]
    somme = 0
    for nombre in liste:
        somme += nombre
    print(f"La somme est : {somme}")
    print(f"La somme est : {somme}")
    # Vulnérabilité potentielle: utilisation de input sans validation
    commande = input("Entrez une commande: ")
    exec(commande)

def autre_demonstration_duplication():
    liste = [1, 2, 3, 4, 5]
    somme = 0
    for nombre in liste:
        somme += nombre
    print(f"La somme est : {somme}")
    # Vulnérabilité potentielle: utilisation de input sans validation
    commande = input("Entrez une commande: ")
    exec(commande)

# Fonction avec plusieurs problèmes
def mauvaises_pratiques():
    # Déclaration de variable non utilisée
    inutilise = "Je ne sers à rien"
    # Opération potentiellement dangereuse
    with open("fichier_inexistant.txt") as fichier:
        contenu = fichier.read()
    print(contenu)

if __name__ == "__main__":
    calcul_inutile()
    demonstration_duplication()
    autre_demonstration_duplication()
    mauvaises_pratiques()
